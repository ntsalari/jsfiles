let session = null;
let username = null;
let tmpgroup = null;

$(document).ready(function() {
	session = Cookies.get('session');
	username = Cookies.get('username');
	if(session == undefined || username == undefined){
		$(location).prop('href', 'login.html');
	}
	setupMenu();
	setup();
	$('#users').on('click', '.clickme', gridmanager);
	$('#newuserform').on('submit', aggiungiUtente);
	$('#newgroupbtn').on('click', aggiungiGruppo);
	$('#groupaddbtn').on('click', userGrpManager);
});

function aggiungiGruppo(){
	let inp = $('#newgroup input[name="groupname"]');
	tmpgroup = inp.val();
	if(tmpgroup != ''){
		$.ajax({
			type: "POST",
			url: "https://salari.xyz/api/gruppi",
			data: JSON.stringify({"groupname": tmpgroup}),
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			},
			success: function(){
				$("#users").find("tr").has("input:checked").each(
					aggiungiAlGruppo);
			}
		});
	}
	tmpgroup = null;
	$("#newgroup").modal('hide');
	inp.val('');
}

function userGrpManager(){
	/*$.ajax({
		type: "POST",
		url: "https://salari.xyz/api/utenti-gruppi",
		data: JSON.stringify({"username": username, "groupname": tmpgroup}),
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			
		}
	});*/
	tmpgroup = $('#usergrpsel').find(':selected').text();
	let azione = $('#actionsel').find(':selected').text();
	if(azione == 'Aggiungi'){
		$("#users").find("tr").has("input:checked").each(
			aggiungiAlGruppo);
	}else if(azione == 'Rimuovi'){
		$("#users").find("tr").has("input:checked").each(
			rimuoviDalGruppo);
	}
	tmpgroup = null;
	$('#groupadd').modal('hide');
}

function aggiungiAlGruppo(){
	let i = $(this).children("td").eq(0).text();
	let invio = {"username": i, "groupname": tmpgroup};
	$.ajax({
		type: "POST",
		url: "https://salari.xyz/api/utenti-gruppi",
		data: JSON.stringify(invio),
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			
		}
	});
	$(this).find("input:checked").prop("checked", false);
}

function rimuoviDalGruppo(){
	let i = $(this).children("td").eq(0).text();
	let invio = {"username": i, "groupname": tmpgroup};
	$.ajax({
		type: "DELETE",
		url: "https://salari.xyz/api/utenti-gruppi",
		data: JSON.stringify(invio),
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			
		}
	});
	$(this).find("input:checked").prop("checked", false);
}

function aggiungiUtente(event){
		event.preventDefault();
		let q = $(this).serializeArray();
		let dati = {};
		for (let i of q){
			dati[i.name] = i.value;
		}
		let invio = JSON.stringify(dati);
		$.ajax({
			type: "POST",
			url: "https://salari.xyz/api/utenti",
			data: invio,
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			},
			success: function(data){
				if($('#tipoutente').is(":checked")){
					$.ajax({
						type: "POST",
						url: "https://salari.xyz/api/amministratori",
						data: invio,
						"headers": {
							"accept": "application/json",
							"Access-Control-Allow-Origin":"https://salari.xyz",
							"session": session
						},
						success: function(data){
							
						}
					});
				}
				$("#newuserform input[type!='submit']").val('');
				$("#tipoutente").prop('checked', false);
				setup();
			}
		});
		$("#newuser").modal('hide');
	}

function gridmanager(){
	let colonna = $(this).parent().children("td").eq(0);
	$.ajax({
		type: "DELETE",
		url: "https://salari.xyz/api/utenti",
		data: JSON.stringify({"username": colonna.text()}),
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			setup();
		}
	});
}

function setupMenu(){
	$.ajax({
		type: "GET",
		url: "https://salari.xyz/api/lista-gruppi",
		crossdomain: "true",
		timeout: 5000,
		data: '',
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			let dati = JSON.parse(data);
			let tmp = ""
			for(let i of dati){
				tmp += "<option>" + i+"</option>";
				$("")
			}
			$("#usergrpsel").html(tmp);
		}
	});
}

function setup(){
	$.ajax({
		type: "GET",
		url: "https://salari.xyz/api/lista-utenti",
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			let tmp = "";
			let dati = JSON.parse(data);
			for(let i of dati){
				tmp += '<tr>\
<td>'+i+'</td>\
<td><input type="checkbox" class="form-check-input"></td>\
<td class="clickme"><i class="fas fa-trash"></i></td>';
			}
			$("#users").html(tmp);
		}
	});
}

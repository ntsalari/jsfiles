$(document).ready(function() {
	$("#loginform").on("submit", 
	function(event){
		event.preventDefault();
		let q = $(this).serializeArray();
		let dati = {};
		for (let i of q){
			dati[i.name] = i.value;
		}
		$.ajax({
			type: "POST",
			url: "https://salari.xyz/api/login",
			crossdomain: "true",
			timeout: 5000,
			data: JSON.stringify(dati),
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz"
			},
			success: function(data){
				let sessione = JSON.parse(data);
				Cookies.set("username", dati["username"]);
				Cookies.set("session", sessione.session);
				$(location).prop('href', 'interface.html');
			},
			statusCode: {
				404: function(){
					$("#logincard").css( "border", "3px solid red" );
				}
			},
			fail: function(){
				$("#logincard").css( "border", "3px solid red" );
			}
		});
	});
	$("#logincard input").on("click", function(){
		$("#logincard").css( "border", "0px" );
	});
});

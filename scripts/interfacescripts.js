let session = null;
let username = null;
let currgroup = null;
let view = 0;
const MAXELEM = 20;

$(document).ready(function() {
	session = Cookies.get('session');
	username = Cookies.get('username');
	if(session == undefined || username == undefined){
		$(location).prop('href', 'login.html');
	}
	
	setup();
	
	setupMenu();
	
	$('#uploadfile').on('click', function(){
		$(this).css( "border", "0px" );
	});
	
	$('#uploadbutton').on('click', uploadmanager);
	
	$('#files').on('click', '.clickme', gridmanager);
	
	$('#grpchangebutton').on('click', setCurrGroup);
	
	$('#filegrpadd').on('click', addtoGroup);
	
	$('#filegrpdel').on('click', delfromGroup);
	
	$('#logoutbtn').on('click', function(){
		logout();
		Cookies.remove('session');
		Cookies.remove('username');
		$(location).prop('href', 'login.html');
	});
	
	$('#prevpage').on('click', function(){
		view = view - MAXELEM;
		if(view < 0){
			view = 0;
		}
		setup();
	});
	
	$('#nextpage').on('click', function(){
		view = view +MAXELEM;
		setup();
	});
	
});

function addtoGroup(){
	let grp = $("#addsel").find(":selected").text();
	$("#files").find("tr").has("input:checked").each(
		function(){
			let i = $(this).children("td").eq(0).text();
			let assoc = {"groupname": grp, "filename": i};
			$.ajax({
				type: "POST",
				url: "https://salari.xyz/api/file-gruppi",
				crossdomain: "true",
				timeout: 5000,
				data: JSON.stringify(assoc),
				"headers": {
					"accept": "application/json",
					"Access-Control-Allow-Origin":"https://salari.xyz",
					"session": session
				},
				success: function(){
					
				},
				statusCode: {
					401: function(){
						alert("Ahia!");
					}
				},
				fail: function(){
					alert("Ahia!");
				}
			});
			$(this).find("input:checked").prop("checked", false);
		});
	$('#groupadd').modal('hide');
}

function delfromGroup(){
	let grp = $("#delsel").find(":selected").text();
	$("#files").find("tr").has("input:checked").each(
		function(){
			let i = $(this).children("td").eq(0).text();
			let assoc = {"groupname": grp, "filename": i};
			$.ajax({
				type: "DELETE",
				url: "https://salari.xyz/api/file-gruppi",
				crossdomain: "true",
				timeout: 5000,
				data: JSON.stringify(assoc),
				"headers": {
					"accept": "application/json",
					"Access-Control-Allow-Origin":"https://salari.xyz",
					"session": session
				},
				success: function(){
					
				},
				statusCode: {
					401: function(){
						alert("Ahia!");
					}
				},
				fail: function(){
					alert("Ahia!");
				}
			});
			$(this).find("input:checked").prop("checked", false);
		});
	$('#groupdel').modal('hide');
}

function gridmanager(){
	let contenuto = $(this).text();
	if(contenuto == ''){
		let colonna = $(this).parent().children("td").eq(0);
		contenuto = colonna.text();
		$.ajax({
			type: "DELETE",
			url: "https://salari.xyz/api/file",
			crossdomain: "true",
			timeout: 5000,
			data: JSON.stringify({"filename": contenuto}),
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			},
			success: function(data){
				setup();
			},
			statusCode: {
			415: function(){
					
				}
			},
			fail: function(){
				
			}
		});
	}else{
		let headers = new Headers();
		headers.append('session', session);
		headers.append('Access-Control-Allow-Origin', "https://salari.xyz/api");
		let owner = $(this).parent().children("td").eq(2).text();
		let extra = "";
		if(owner != username){
			extra += "&owner="+owner;
		}
		fetch("https://salari.xyz/api/file?filename="+contenuto+extra, {
			method: 'GET',
			mode: 'cors',
			headers: headers
		})
		.then(response => response.blob())
		.then(blob => {
			let url = window.URL.createObjectURL(blob);
			let a = document.createElement('a');
			a.href = url;
			a.download = contenuto;
			document.body.appendChild(a);
			a.click();
			a.remove();
			window.URL.revokeObjectURL(url);      
		});
	}
}

function uploadmanager(){
		let f = $('#uploadfile').prop("files");
		if(f[0] != undefined){
			for(let i of f){
				let dati = new FormData();
				dati.append('file', i);
				$.ajax({
					type: "POST",
					url: "https://salari.xyz/api/file",
					enctype: 'multipart/form-data',
					crossdomain: "true",
					timeout: 3600000, //Un ora in ms
					data: dati,
					cache: false,
					contentType: false,
					processData: false,
					"headers": {
						"accept": "application/json",
						"Access-Control-Allow-Origin":"https://salari.xyz",
						"session": session
					},
					success: function(data){
						setup();
					},
					statusCode: {
						415: function(){
							
						}
					},
					fail: function(){
						
					}
				});
			}
			$('#uploader').modal('hide');
			$('#uploadfile').val('');
		}else{
			$("#uploadfile").css( "border", "3px solid red" );
		}
}

function formatBytes(bytes, decimals = 2) {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function setupLista(data){
	let dati = JSON.parse(data);
	let tmp = "";
	let nome = username;
	while(view >= dati.length){
		view = view - MAXELEM;
	}
	if(dati.length == 0){
		return;
	}
	let maxview = Math.min(dati.length, view + MAXELEM);
	for (let k = view; k < maxview; k++){
		let i = dati[k];
		if(i.hasOwnProperty("owner")){
			nome = i["owner"];
		}else{
			nome = username;
		}
		tmp += '<tr>\
<td class="clickme"><i class="fas fa-file"></i>\
'+i["filename"]+'</td>\
<td>'+formatBytes(i['filesize'])+'</td>';
		if(nome == username){
			tmp += '<td>'+nome+'</td>\
<td><input type="checkbox" class="form-check-input"></td>\
<td class="clickme"><i class="fas fa-trash"></i></td>';
		}else{
			tmp += '<td colspan="3">'+nome+'</td>';
		}
		tmp += '</tr>';
	}
	$("#files").html(tmp);
}

function setCurrGroup(){
	sel = $("#grpsel").find(":selected").text();
	if(sel != "I miei files"){
		currgroup = sel;
	}else{
		currgroup = null;
	}
	$('#groupchanger').modal('hide');
	view = 0;
	setup();
}

function setupMenu(){
	$.ajax({
		type: "GET",
		url: "https://salari.xyz/api/lista-amministratori",
		crossdomain: "true",
		timeout: 5000,
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			let admin = false;
			let dati = JSON.parse(data);
			for (let i of dati){
				if(username == i){
					admin = true;
					break;
				}
			}
			if(admin){
				let pulsante = $('<button class="btn btn-primary"/>');
				pulsante.text("Manager");
				pulsante.on('click', function(){
					$(location).prop('href', 'manager.html');
				});
				$('#menutab').append(pulsante);
			}else{
				$('#menutab').find('button.disablebtn').prop('disabled', true);
			}
		},
		statusCode: {
			401: function(){
				alert("Ahia!");
			}
		},
		fail: function(){
			alert("Ahia!");
		}
	});
	$.ajax({
		type: "GET",
		url: "https://salari.xyz/api/lista-gruppi",
		crossdomain: "true",
		timeout: 5000,
		data: 'username='+username,
		"headers": {
			"accept": "application/json",
			"Access-Control-Allow-Origin":"https://salari.xyz",
			"session": session
		},
		success: function(data){
			let dati = JSON.parse(data);
			let tmp = ""
			for(let i of dati){
				tmp += "<option>" + i+"</option>";
				$("")
			}
			$("#grpsel").append(tmp);
			$("#addsel").html(tmp);
			$("#delsel").html(tmp);
		},
		statusCode: {
			401: function(){
				alert("Ahia!");
			}
		},
		fail: function(){
			alert("Ahia!");
		}
	});
}

function setup(){
	if(currgroup == null){
		$.ajax({
			type: "GET",
			url: "https://salari.xyz/api/lista-file",
			crossdomain: "true",
			timeout: 5000,
			//data: "",
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			},
			success: setupLista,
			statusCode: {
				401: function(){
					alert("Ahia!");
				}
			},
			fail: function(){
				alert("Ahia!");
			}
		});
	}else{
		$.ajax({
			type: "GET",
			url: "https://salari.xyz/api/file-gruppi",
			crossdomain: "true",
			timeout: 5000,
			data: "groupname="+currgroup,
			"headers": {
				"accept": "application/json",
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			},
			success: setupLista,
			statusCode: {
				401: function(){
					alert("Ahia!");
				}
			},
			fail: function(){
				alert("Ahia!");
			}
		});
	}
}

function logout(){
	$.ajax({
			type: "GET",
			url: "https://salari.xyz/api/logout",
			crossdomain: "true",
			timeout: 5000,
			"headers": {
				"Access-Control-Allow-Origin":"https://salari.xyz",
				"session": session
			}
		});
	session = null;
	username = null;
	currgroup = null;
}
